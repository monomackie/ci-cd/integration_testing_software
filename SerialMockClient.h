#pragma once

#include <SoundController.h>
#include "SerialPort.h"

#define SMC_STATE_DISCONNECTED      0
#define SMC_STATE_CONNECTED         1

struct SerialData {
    unsigned int type;
    unsigned int data;
};

class SerialMockClient {
private:
    unsigned int    m_State;
    SerialPort      m_SerialPort;
    SoundController m_SoundController;
    int             m_SelectedSession;
    int             m_MistakesCount;
public:
    explicit SerialMockClient(const SerialPort& serialPort);
    ~SerialMockClient();

    void pingToClient();
    void sendDataToClient(SerialData sd);
    void    leftButtonClick();
    void    rightButtonClick();
    void    movePotentiometerTo(int value);
    int     getSelectedSession() const;
    int     getMistakesCount() const;
    unsigned int getState() const;
    static void waitMillis(int millis);

    void checkTargetVolume(int target, int waitTime);
};
