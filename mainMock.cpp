#include <stdexcept>
#include "SerialPort.h"
#include "SerialMockClient.h"
#include "Log.h"
#include "Infotag.h"

char port[] = "COM7";
int baudrate = 9600;
int numberOfBits = 8;
int stopBits = 1;
SerialPortParity parity = SerialPortParity::SPP_NO_PARITY;
bool flowControl = false;
unsigned int timeout = 1000;

void standardTest(SerialMockClient& serialMockClient, int volume, int delay, int button) {
    if (button < 0) {
        while (button < 0) {
            button++;
            serialMockClient.leftButtonClick();
        }
    }

    else if (button > 0) {
        while (button > 0) {
            button--;
            serialMockClient.rightButtonClick();
        }
    }

    serialMockClient.movePotentiometerTo(volume);
    SerialMockClient::waitMillis(delay);
    serialMockClient.checkTargetVolume(volume, delay);
}

void sendTests(SerialMockClient& serialMockClient) {
    standardTest(serialMockClient, 1023, 50,  0);
    standardTest(serialMockClient,  512, 50,  0);
    standardTest(serialMockClient,  225, 50,  1);
    standardTest(serialMockClient,  400, 50,  1);
    standardTest(serialMockClient, 1023, 50, -1);
    standardTest(serialMockClient,  839, 50, -1);
    standardTest(serialMockClient, 1023, 50,  2);
    standardTest(serialMockClient, 1023, 50, -2);
}

int main() {
	SerialPort *serialPort;
	try {
		serialPort = new SerialPort(port, timeout, baudrate, numberOfBits, stopBits, parity, flowControl);
	} catch (const std::runtime_error& e) {
		Log::printStackTrace(e);
		return -1;
	}

	Log::info("<%s> Connected to port %s !\n", INFOTAG_PORT_CONNECTED, port);
	SerialMockClient serialMockClient(*serialPort);

	while(serialMockClient.getState()==SMC_STATE_DISCONNECTED) {
		serialMockClient.pingToClient();
	}

	if (serialMockClient.getState() == SMC_STATE_CONNECTED) {
		Log::info("<%s> Connection established with software\n", INFOTAG_SOFTWARE_CONNECTED);
		sendTests(serialMockClient);
		Log::info("<%s> Number of error(s): %d\n", INFOTAG_TOTAL_ERRORS, serialMockClient.getMistakesCount());
	}


    return 0;
}