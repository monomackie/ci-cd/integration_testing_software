# Integration testing

This repo is for integration (or functionnal) testing on the **software** side of the Monomackie project.

In order to do that, we isolate the project and control its inputs (serial port), and check if we get the ouputs we expect (on serial port and on sound settings of the OS).

```mermaid
graph LR
    IT[Integration Testing] 
    M(Monomackie)
    OS[Windows OS]

    IT -->|Virtual serial| M
    M -->|Virtual serial| IT

    M -->|WinAPI| OS
    OS --> |WinAPI| IT
```

## Setup virtual COM port

Using a virtual COM port to send data to the software

Prior to compiling, you must install a signed com0com driver which you can download on [Pete's website](https://pete.akeo.ie/2011/07/com0com-signed-drivers.html)

From an elevated terminal, you must then execute 'setupc.exe' and run the following command :
```shell
install PortName=COM7 PortName=COM8
```

To make sure the virtual ports are created, run :
```
list
```
you should see the newly created ports

## Compile

With Cmake and MSVC.

## Run

For now you need to run the exectuble and check the output logs.

We expect to use a framework like catch2 or gtest in the futur.