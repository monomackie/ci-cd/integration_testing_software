cmake_minimum_required(VERSION 3.20)
project(mock_serial)

set(CMAKE_CXX_STANDARD 14)
set(EXCEPTION_PATH "../../external/exception")

add_subdirectory(external/exception)
add_subdirectory(external/serial_protocol)
add_subdirectory(external/serial_communication)
add_subdirectory(external/sound_control)
add_subdirectory(external/log)

add_executable(mock_serial
        mainMock.cpp
        SerialMockClient.cpp
        SerialMockClient.h
        Infotag.h
        )

target_include_directories(mock_serial PUBLIC external/exception)
target_include_directories(mock_serial PUBLIC external/serial_protocol)
target_include_directories(mock_serial PUBLIC external/serial_communication)
target_include_directories(mock_serial PUBLIC external/sound_control)
target_include_directories(mock_serial PUBLIC external/log)

target_link_libraries(mock_serial PUBLIC exception serial_communication sound_control log)
